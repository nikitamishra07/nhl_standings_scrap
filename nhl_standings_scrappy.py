import json
import os

import pandas as pd
import requests
from pandas.io.json import json_normalize

DATA_DIRECTORY = f'data{os.sep}'

LOCAL_FILENAME_ALL_STANDINGS = 'nhl_standings_overall.csv'
LOCAL_FILENAME_DATED_STANDINGS = 'nhl_standings_dated.csv'
LOCAL_FILENAME_DIFF = 'nhl_standings_diff.csv'

URL = 'https://statsapi.web.nhl.com/api/v1/standings?expand=standings.record'
DATE_URL = f'{URL}&date=2018-02-26'

RELEVANT_COLUMNS = ['team.name', 'wildCardRank', 'gamesPlayed', 'points', 'leagueRecord.wins', 'row',
                    'leagueRecord.losses', 'leagueRecord.ot', 'records.overallRecords2wins',
                    'records.overallRecords2losses', 'goalsScored', 'goalsAgainst']
RELEVANT_COLUMNS_RENAMED = ['Team', 'WC', 'GP', 'P', 'W', 'ROW', 'L', 'OTL', 'SOW', 'SOL', 'GF', 'GA']
DIFF_COLUMN_NAMES = ['GP', 'P', 'W', 'ROW', 'L', 'OTL', 'SOW', 'SOL', 'GF', 'GA', 'GD']
ORDERED_COLUMNS = ['WC', 'GP', 'P', 'P%', 'W', 'ROW', 'L', 'OTL', 'SOW', 'SOL', 'GF', 'GA', 'GD']


def get_data_from_nhl_stats():
    data_all = _get_data_for_nhl_stats_from_url(URL, LOCAL_FILENAME_ALL_STANDINGS)
    data_dated = _get_data_for_nhl_stats_from_url(DATE_URL, LOCAL_FILENAME_DATED_STANDINGS)

    diff = _diff_data(data_all, data_dated)
    return diff


def _diff_data(data_all: pd.DataFrame, data_dated: pd.DataFrame) -> pd.DataFrame:
    data_all_subset = data_all[DIFF_COLUMN_NAMES]
    data_dated_subset = data_dated[DIFF_COLUMN_NAMES]

    # Take diff of the data, datasets are already aligned because of the index
    difference_table = data_all_subset.subtract(data_dated_subset, fill_value=0)

    # Add WC as a column
    difference_table['WC'] = data_all['WC']

    # Update with P % and GD
    difference_table = _add_gd_and_p_percent(difference_table)

    # Output data to a csv file
    _to_csv(difference_table, LOCAL_FILENAME_DIFF)

    return difference_table


def _get_data_for_nhl_stats_from_url(url: str, local_filename: str) -> pd.DataFrame:
    """
    Get data from statsapi.nhl and process it to a pandas dataframe and output it as a csv file.
    """

    # Read data from the api
    nhl_raw_data = requests.get(url).content

    # Decode data
    decoded_nhl_data = nhl_raw_data.decode('utf8')
    decoded_nhl_json = json.loads(decoded_nhl_data)

    # Process data division wise
    nhl_table = _process_json_to_dataframe(decoded_nhl_json)

    # Output data to a csv file
    _to_csv(nhl_table, local_filename)

    return nhl_table


def _to_csv(data: pd.DataFrame, local_filename: str):

    # Create nested directories if they don't exist
    if not os.path.exists(DATA_DIRECTORY):
        os.makedirs(DATA_DIRECTORY)

    # Get absolute file path
    absolute_filename = os.path.join(DATA_DIRECTORY, local_filename)

    # Write to csv file
    data.to_csv(absolute_filename)


def _process_json_to_dataframe(decoded_json: json) -> pd.DataFrame:

    nhl_table = [_get_data_for_division(decoded_json, item) for item in range(4)]
    nhl_table = pd.concat(nhl_table, axis=0)

    # Reorder columns
    nhl_table = nhl_table[RELEVANT_COLUMNS]

    # Rename columns and set Team name as index and sort by the team name
    nhl_table.columns = RELEVANT_COLUMNS_RENAMED
    nhl_table.sort_values('Team', inplace=True)
    nhl_table.set_index('Team', inplace=True)

    # Add calculations
    nhl_table = _add_gd_and_p_percent(nhl_table)

    return nhl_table


def _add_gd_and_p_percent(nhl_table: pd.DataFrame) -> pd.DataFrame:
    # Final ordering after calculations are made and the team name is set as the index.

    # Calculate GD
    nhl_table['GD'] = nhl_table['GF'] - nhl_table['GA'] - nhl_table['SOW'] + nhl_table['SOL']

    # Calculate P%
    nhl_table['P%'] = round(nhl_table['P'] / (2 * nhl_table['GP']), 3)

    # Reorder columns again!
    nhl_table = nhl_table[ORDERED_COLUMNS]

    return nhl_table


def _expand_list_of_dictionaries_into_multiple_columns(df, column_name):
    expanded_df = _expand_column_list_into_multiple_columns(df, column_name)

    a = []
    for column in expanded_df:
        a.append(_expand_column_dict_into_multiple_columns(expanded_df, column))

    appended_data = pd.concat(a, axis=1)
    return appended_data


def _expand_column_list_into_multiple_columns(df, column_name):
    entry0 = df[column_name][0]
    expanded_column_names = [column_name + str(i) for i in range(len(entry0))]
    df = pd.DataFrame(df[column_name].values.tolist(), columns=expanded_column_names)
    return df


def _expand_column_dict_into_multiple_columns(df, column_name):
    expanded_df = pd.DataFrame(df[column_name].tolist())
    expanded_column_names = [column_name + str(i) for i in expanded_df]
    expanded_df.columns = expanded_column_names
    return expanded_df


def _get_data_for_division(decoded_json, division_id):
    df = json_normalize(decoded_json['records'][division_id]['teamRecords'])
    df_records_overallRecords = _expand_list_of_dictionaries_into_multiple_columns(df, 'records.overallRecords')
    final_df = pd.concat([df, df_records_overallRecords], axis=1)
    return final_df


def main():
    final_table = get_data_from_nhl_stats()
    print(final_table)


if __name__ == "__main__":
    main()